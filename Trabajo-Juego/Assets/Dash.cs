﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Dash : MonoBehaviour
{

    ControlPlayer Movimiento;
    ControlCamara Mirar;

    public float tiempoDash;
    public float velocidadDash;

    void Start()
    {
        Movimiento = GetComponent<ControlPlayer>();
        Mirar = GetComponent<ControlCamara>();
    }


    void Update()
    {
        if(Input.GetKeyDown(KeyCode.V))
        {
            StartCoroutine(Dashh());

        }
    }

    IEnumerator Dashh()
    {
        float startTime = Time.time;

        while(Time.time<startTime + tiempoDash)
        {
            Movimiento.transform.Translate(Vector3.forward,gameObject.transform);
            yield return null;
        }
    }
}
