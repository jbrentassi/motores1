﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Granada : MonoBehaviour
{

    private int cubesPerAxis = 8;
    private float delay = 0.5f;
    private float force = 350f;
    private float radius = 5f;
    Rigidbody r;

    void Start()
    {

        r = GetComponent<Rigidbody>();

    }

    void main()
    {

        for (int x = 0; x < cubesPerAxis; x++)
        {

            for (int y = 0; y < cubesPerAxis; y++)
            {

                for (int z = 0; z < cubesPerAxis; z++)
                {

                    CreateCube(new Vector3(x, y, z));

                }

            }

        }

        Destroy(gameObject);

    }


    void Update()
    {

        if(TirarGranada.tiro == true)
        {

            r.AddForce(transform.forward * 10);

        }

    }

    public void CreateCube(Vector3 coordinates)
    {

        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);

        Renderer rd = cube.GetComponent<Renderer>();
        rd.material = GetComponent<Renderer>().material;

        cube.transform.localScale = transform.localScale / cubesPerAxis;

        Vector3 firstCube = transform.position - transform.localScale / 2 + cube.transform.localScale / 2;
        cube.transform.position = firstCube + Vector3.Scale(coordinates, cube.transform.localScale);

        Rigidbody r = cube.AddComponent<Rigidbody>();
        r.AddExplosionForce(force, transform.position, radius);

        cube.tag = "Granada";

        Destroy(cube, 2);

    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("Piso"))
        {

            Invoke("main", delay);

        }

    }

}