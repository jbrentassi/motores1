﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlPlayer : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    private Rigidbody rb;
    private Rigidbody r;
    public GameObject proyectil;
    public Camera camaraPrimeraPersona;    
    public float seg = 0f;
    private int Vida = 100;
    public GameObject granade;
    //private int cont = 3;

    


    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        r = granade.GetComponent<Rigidbody>();
    }

    public void desaparecer()
    {
        SceneManager.LoadScene("EndGame");
    }

    

    private void OnCollisionEnter(Collision collision)
    {
         

         if(collision.gameObject.CompareTag("Pincho"))
         {
              Vida = Vida - 25;
         }
    }

    




    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

          
        
        if (Vida == 0)
        {
            desaparecer();
        }
        

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (Input.GetMouseButtonDown(0))
        {

            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse);

            Destroy(pro, 2);

            GestorAudio.instancia.ReproducirSonido("Shoot");

            Invoke("MostrarExplosion", seg);

        }

        //if (Input.GetMouseButtonDown(1) && cont > 0)
        //{
        //
        //    Instantiate(granade, gameObject.transform.position, gameObject.transform.rotation);
        //
        //    r.AddForce(camaraPrimeraPersona.transform.forward * 10, ForceMode.Impulse);
        //    
        //    cont = cont - 1;
        //
        //}

    }
   
}
