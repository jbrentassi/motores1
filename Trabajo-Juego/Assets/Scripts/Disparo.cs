﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    
    public GameObject ExplocionDisparo;


    void Start()
    {
        
    }

    
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            MostrarExplosion();
        }
    }

    public void MostrarExplosion()
    {
        GameObject particulas = Instantiate(ExplocionDisparo, transform.position, Quaternion.identity) as GameObject;
        Destroy(particulas, 1);
    }
}
