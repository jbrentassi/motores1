﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TirarGranada : MonoBehaviour
{
    public GameObject granade;
    private int cont = 10;
    private Rigidbody r;
    public static bool tiro = false;

    void Start()
    {
        r = granade.GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1) && cont > 0)
        {

            Instantiate(granade, gameObject.transform.position, gameObject.transform.rotation);

            r.AddForce(granade.transform.forward * 10);

            cont = cont - 1;

            tiro = true;

        }
    }
}
