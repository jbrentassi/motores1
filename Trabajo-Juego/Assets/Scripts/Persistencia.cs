﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class Persistencia : MonoBehaviour
{
    public Persistencia instancia;
    public DataPersistencia data;
    public string savedata = "save.dat";
    public GameObject _player;
    private void Awake()
    {
        if (instancia is null)
        {
            DontDestroyOnLoad(this.gameObject);
            instancia = this;
        }
        else if (instancia != this)
            Destroy(this.gameObject);

        LoadData();

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            SaveData();
        }

    }
    public void SaveData()
    {
        data.posicion = new Punto(_player.transform.position);
        string filePath = Application.persistentDataPath + "/" + savedata;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Datos Guardados");

    }
    public void LoadData()
    {
        string filePath = Application.persistentDataPath + "/" + savedata;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            DataPersistencia cargado = (DataPersistencia)bf.Deserialize(file);
            data = cargado;
            _player.transform.position = new Vector3(data.posicion.x, data.posicion.y, data.posicion.z);
            
            file.Close();
            Debug.Log("Datos cargados");


           
        }
    }

    [System.Serializable]
    public class DataPersistencia
    {
        public Punto posicion;
    }
    [System.Serializable]

    public class Punto
    {

        public float x;
        public float y;
        public float z;


        public Punto(Vector3 p)
        {
            x = p.x;
            y = p.y;
            z = p.z;
        }


        public Vector3 aVector()
        {
            return new Vector3(x, y, z);
        }



    }
}
