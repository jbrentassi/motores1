﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salto : MonoBehaviour
{
    public Vector3 direccion;
    public float velocidad;
    public bool tocandoPiso;
    public bool dobleSalto;
    bool SaltoEnUso;

    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {

    }

    private void FixedUpdate()
    {
        if (Input.GetAxisRaw("Jump") == 1)
        {
            if (!SaltoEnUso)
            {
                if (tocandoPiso)
                {
                    rb.AddForce(Vector3.up * 7, ForceMode.Impulse);
                    tocandoPiso = false;
                }
                else
                {
                    if (dobleSalto)
                    {
                        rb.AddForce(Vector3.up * 10, ForceMode.Impulse);
                        dobleSalto = false;
                    }
                }
                SaltoEnUso = true;
            }
        }
        else if (Input.GetAxisRaw("Jump") == 0)
        {
            SaltoEnUso = false;

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Piso")
        {
            tocandoPiso = true;
            dobleSalto = false;
        }
    }

}
