﻿using UnityEngine;

public class Bot_Enemigo : MonoBehaviour
{
    public int hp = 100;
    private GameObject jugador;
    public float rapidez;
    public GameObject target;
    public static int contZom = 0;


    void Start()
    {
        
    }

    public void recibirDaño()
    {
        hp = hp - 25;

        if (hp <= 0)
        {
            this.desaparecer();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {

            recibirDaño();

        }
        if (collision.gameObject.CompareTag("Granada"))
        {

            this.desaparecer();

        }
    }




private void desaparecer()
    {
        Destroy(gameObject);
        contZom = contZom + 1;
    }



    void Update()
    {
        transform.LookAt(target.transform);

        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);

        if(Vector3.Distance(transform.position, target.transform.position) < 5)
        {
            target = GameObject.Find("Jugador");
        }

    }
}
