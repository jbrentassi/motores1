﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
     
    public GameObject target;
    public Animator ani;

    void Start()
    {
        target = GameObject.Find("Jugador");
        //ani = GameObject.GetComponent<Animator>();
    }

    public void ActivarPuerta()
    {
        if(Vector3.Distance(transform.position, target.transform.position) < 10)
        {
            ani.SetBool("Abrir", true);
        }else
        {
            ani.SetBool("Abrir", false);
        }
    }
    void Update()
    {
        ActivarPuerta();
    }
}